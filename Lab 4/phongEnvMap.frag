// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;

uniform samplerCube cubeMap;
uniform sampler2D texMap;
in vec3 ex_WorldNorm;
in vec3 ex_WorldView;

in float ex_attenuation;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;
layout(location = 0) out vec4 out_Color;


void main(void) {

	// Attenuation old
	//float att = 1.0/( constant + (linear*ex_Distance) + (quadratic * ex_Distance*ex_Distance ) );
	//vec4 newatt = vec4(att, att, att, 1.0);

	// Attenuation
	//float attenuation = attConst + attLinear * ex_Distance + attQuadratic * ex_Distance*ex_Distance;

	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);

	vec4 tmp_Color = (ambientI + diffuseI + specularI);
	vec4 litColour = vec4(tmp_Color.rgb / ex_attenuation, 1.0);

	vec3 reflectTexCoord = reflect(-ex_WorldView, normalize(ex_WorldNorm));
	

	// Fragment colour
	//out_Color = litColour * texture(textureUnit0, ex_TexCoord);
	out_Color = texture(cubeMap, reflectTexCoord) * texture2D(texMap, ex_TexCoord) * litColour;
	//out_Color = (newatt*(ambientI + diffuseI + specularI)) * texture(textureUnit0, ex_TexCoord);
	//out_Color = texture2D(textureUnit0, ex_TexCoord);
}