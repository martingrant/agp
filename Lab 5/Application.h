#pragma once
#ifndef APPLICATION_H
#define APPLICATION_H

#define DEG_TO_RADIAN 0.017453293

#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "rt3d.h"
#include <stack>
#include "md2model.h"
#include "rt3dObjLoader.h"
#include <SDL_ttf.h>

using namespace std;

class Application
{
public:
	Application(void);
	~Application(void);

	void init();
	SDL_Window * setupRC(SDL_GLContext &context);
	void run();
	void update();
	void draw(SDL_Window * window);

	GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */);
	GLuint loadBitmap(char *fname);
	GLuint loadCubeMap(const char *fname[6], GLuint *texID);

	void setUniform(GLuint shader, const char * uniform, GLfloat val);
	void setUniform(GLuint shader, const char * uniform, GLfloat val, glm::vec3 iEye);

	glm::vec3 moveForward(glm::vec3 pos, GLfloat angle, GLfloat d);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);

private:
	SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle

	GLuint meshIndexCount;
	GLuint toonIndexCount;
	GLuint dragonIndexCount;
	GLuint md2VertCount;
	GLuint meshObjects[6];

	GLfloat r;

	GLfloat theta;

	GLuint shaderProgram;
	GLuint skyboxProgram;
	GLuint toonShaderProgram;
	GLuint reflectionMapProgram;

	rt3d::lightStruct light0;
	rt3d::materialStruct material0;
	rt3d::materialStruct material1;

	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;

	glm::vec4 lightPos;
	glm::vec3 boxPos;

	stack<glm::mat4> mvStack; 

	GLuint textures[3];
	GLuint skybox[5];
	GLuint labels[5];

	GLfloat attConstant;
	GLfloat attLinear;
	GLfloat attQuadratic;

	md2model tmpModel;
	int currentAnim;

	TTF_Font * textFont;

	// PBO example variables
	GLuint texShaderProgram;
	GLuint pboTex;
	GLuint pboTexSize;
	GLuint pboBuffer;
	GLuint screenHeight;
	GLuint screenWidth;

	// FBO example variables
	GLuint fboID;
	GLuint depthBuffID;
	GLuint reflectionTex;
	GLuint reflectionWidth;
	GLuint reflectionHeight;
	//static const GLenum fboAttachments[];
	//static const GLenum frameBuff[];
	GLfloat mirrorAngle;
};
#endif
