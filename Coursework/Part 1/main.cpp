// UWS AGP 2013-2014 Coursework Part 1
// B00221736

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Application.h"

using namespace std;

// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
  
	Application app;

	app.init();
	app.run();
	app.~Application();
    
    return 0;
}