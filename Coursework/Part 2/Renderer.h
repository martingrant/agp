#pragma once

#define NUMLIGHTS 8

/////////// Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
// How many frames time values to keep
// The higher the value the smoother the result is...
// Don't make it 0 or less :)
#define FRAME_VALUES 10
///////////

#include "rt3dObjLoader.h"
#include "rt3d.h"
#include "Shaders.h"
#include "particlesList.h"
#include <unordered_map>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <stack>
#include <vector>
#include <sstream> 
#include <ctime>
#include "Light.h"
#include <SDL_ttf.h>

using namespace std;

typedef unordered_map<std::string, GLuint> texturemap;

class Renderer
{
public:
	Renderer();
	~Renderer();

	void init();
	void update();

	void renderDepth(glm::vec3 coords, glm::vec3 size, glm::vec4 iLightPos, glm::vec3 iLightConeDirection); 
	void renderProp(string textureID, glm::vec3 coords, glm::vec3 size,glm::vec3 iLightConeDirection[], Light iLightList[], glm::vec4 iLightPosList[] );
	void renderLightProps(glm::vec4 lightPosition);
	void renderLight(Light iLightList[], glm::vec4 iLightPosList[]);
	void renderParticles(string ID, GLfloat size, glm::vec4 iLightPos);
	
	void setDepthFBO();

	void setUniform(GLuint shader, const char * uniform, int val); // sends texture to shader
	void setUniform(GLuint shader, const char * uniform, glm::vec3 val);	// used for passing attenuation values to shader
	void setUniform(GLuint shader, Light iLightList[]);	// used for passing light array to shader
	void setUniform(GLuint shader, GLfloat val1, GLfloat val2, glm::vec3 val3); // used for passing fog values to shader
	void setUniformMatrix3fv(const GLuint program, const char * uniform, const GLfloat *data);

	void renderHUD();
	GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h*/);
	void initFPSTimer();
	void Renderer::updateFPSTimer();


	ParticlesList * getParticleLight();
	
	glm::vec3 getEye();
	GLfloat getRotate();
	glm::vec3 getParticleCubePosition();
	GLuint getFBO();

	void setEye(glm::vec3 newEye);
	void setRotate(GLfloat newRotate);
	void setParticleCubePosition(glm::vec3 newParticleCubePosition);

private:
	Shaders shaderManager;
	texturemap textureMap;

	GLuint meshIndexCount;
	GLuint meshObjects[2]; // these are example sizes not all are used yet

	glm::mat4 projection;
	stack<glm::mat4> mvStack;
	GLuint size;

	vector<GLfloat> cubeVerts;
	vector<GLfloat> cubeNorms;
	vector<GLfloat> cubeTex_coords;
	vector<GLuint> cubeIndices;
	vector<GLfloat> sphereVerts;
	vector<GLfloat> sphereNorms;
	vector<GLfloat> sphereTex_coords;
	vector<GLuint> sphereIndices;

	GLuint depthFBO;
	GLfloat rotate;

	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;

	rt3d::lightStruct light0;
	glm::vec3 particleCubePosition;

	rt3d::materialStruct material0;
	rt3d::materialStruct material1;

	glm::mat4 biasMatrix;

	glm::vec3 attenuation;

	ParticlesList * particleLight;

	TTF_Font * textFont;
	GLuint labels[2];
	glm::vec3 HUDPositions[2];
	glm::vec3 HUDSizes[2];
	
	//////// // Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
	// An array to store frame times:
	Uint32 frametimes[FRAME_VALUES];

	// Last calculated SDL_GetTicks
	Uint32 frametimelast;

	// total frames rendered
	Uint32 framecount;

	// the value you want
	float framespersecond;
	////////

	// fog
	GLfloat fogMaxDist;
	GLfloat fogMinDist;
	glm::vec3 fogColour;

	glm::mat4 shadow;
};

