 #include "GameScreen.h"

GameScreen::GameScreen()
{
}

GameScreen::~GameScreen()
{
}

void GameScreen::init() 
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);	
	std::srand(time(0));
	renderer.init();
	renderer.initFPSTimer();
	renderer.setParticleCubePosition(glm::vec3(0.0f,10.0f,0.0f));

	// light object constructors
	lightList[0] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.5f, 0.0f, 0.0f, 1.0f), glm::vec4(0.1f, 0.0f, 0.0f, 1.0f), glm::vec4(-8.0f, 51.0f, -42.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 1);
	lightList[1] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.5f, 0.0f, 1.0f), glm::vec4(0.0f, 0.1f, 0.0f, 1.0f), glm::vec4(30.0f, 10.0f, 0.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 1);
	lightList[2] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 0.5f, 1.0f), glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(15.0f, 10.0f, -20.0f, 1.0f),glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 0);
	lightList[3] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.5f, 0.1f, 0.0f, 1.0f), glm::vec4(0.1f, 0.1f, 0.0f, 1.0f), glm::vec4(10.0f, 10.0f, 5.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 1);
	lightList[4] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.5f, 0.1f, 0.5f, 1.0f), glm::vec4(0.5f, 0.1f, 0.5f, 1.0f), glm::vec4(-60.0f,12.0f,-15.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 1);
	lightList[5] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.5f, 0.0f, 0.0f, 1.0f), glm::vec4(0.5f, 0.0f, 0.0f, 1.0f), glm::vec4(-35.0f,10.0f,-20.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 0);
	lightList[6] = Light(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.3f, 0.0f, 0.3f, 1.0f), glm::vec4(0.1f, 0.0f, 0.1f, 1.0f), glm::vec4(-48.0f,6.0f,0.0f, 1.0f), glm::vec3(0.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 0, 1);
	lightList[7] = Light(glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), glm::vec4(0.2f, 1.0f, 0.2f, 1.0f), glm::vec4(0.3f, 0.9f, 0.2f, 1.0f), glm::vec4(renderer.getParticleCubePosition().x,renderer.getParticleCubePosition().y,renderer.getParticleCubePosition().z, 1.0f), glm::vec3(30.01, -0.15, 0.009),1.39, glm::vec3(1.0f, 0.01f, 0.001f), 1, 0);
	
	// light position in world space
	lightPosList[0] = glm::vec4(-8.0f, 51.0f, -42.0f, 1.0f);
	lightPosList[1] = glm::vec4(30.0f, 10.0f, 0.0f, 1.0f);
	lightPosList[2] = glm::vec4(15.0f, 10.0f, -20.0f, 1.0f);
	lightPosList[3] = glm::vec4(10.0f, 10.0f, 5.0f, 1.0f);
	lightPosList[4] = glm::vec4(-60.0f,12.0f,-15.0f, 1.0f);
	lightPosList[5] = glm::vec4(-35.0f,10.0f,-20.0f, 1.0f);
	lightPosList[6] = glm::vec4(-48.0f,6.0f,0.0f, 1.0f);
	lightPosList[7] = glm::vec4(renderer.getParticleCubePosition().x,renderer.getParticleCubePosition().y,renderer.getParticleCubePosition().z, 1.0f);

	// light cone for spotlights
	lightConeDirection[0] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[1] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[2] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[3] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[4] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[5] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[6] = glm::vec3(0.01, -1.41, -1.00);
	lightConeDirection[7] = glm::vec3(0.01, -1.41, -1.00);

	selectLight = 0;
}

bool GameScreen::update()
{
	bool running = true;
	
	
	renderer.update();

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	// moving camera 
	if ( keys[SDL_SCANCODE_W] ) renderer.setEye(rt3d::moveForward(renderer.getEye(), renderer.getRotate(), 0.5f));
	if ( keys[SDL_SCANCODE_S] ) renderer.setEye(rt3d::moveForward(renderer.getEye(), renderer.getRotate(), -0.5f));
	if ( keys[SDL_SCANCODE_A] ) renderer.setEye(rt3d::moveRight(renderer.getEye(), renderer.getRotate(), -0.5f));
	if ( keys[SDL_SCANCODE_D] ) renderer.setEye(rt3d::moveRight(renderer.getEye(), renderer.getRotate(), 0.5f));
	if ( keys[SDL_SCANCODE_R] ) renderer.setEye(glm::vec3(renderer.getEye().x, renderer.getEye().y + 0.5, renderer.getEye().z));
	if ( keys[SDL_SCANCODE_F] ) renderer.setEye(glm::vec3(renderer.getEye().x, renderer.getEye().y - 0.5, renderer.getEye().z));
	
	// rotate camera
	if ( keys[SDL_SCANCODE_COMMA] )renderer.setRotate(renderer.getRotate() - 1.0f);
	if ( keys[SDL_SCANCODE_PERIOD] ) renderer.setRotate(renderer.getRotate() + 1.0f);
	
	// select light
	if ( keys[SDL_SCANCODE_1] ) selectLight = 0;
	if ( keys[SDL_SCANCODE_2] ) selectLight = 1;
	if ( keys[SDL_SCANCODE_3] ) selectLight = 2;
	if ( keys[SDL_SCANCODE_4] ) selectLight = 3;
	if ( keys[SDL_SCANCODE_5] ) selectLight = 4;
	if ( keys[SDL_SCANCODE_6] ) selectLight = 5;
	if ( keys[SDL_SCANCODE_7] ) selectLight = 6;
	if ( keys[SDL_SCANCODE_8] ) selectLight = 7;
	// set attenuation for chosen light
	if ( keys[SDL_SCANCODE_O] ) lightList[selectLight].setAttenuationLinear(lightList[selectLight].getAttenuationLinear() + 0.01f);
	if ( keys[SDL_SCANCODE_P] ) lightList[selectLight].setAttenuationLinear(lightList[selectLight].getAttenuationLinear() - 0.01f);
	if ( keys[SDL_SCANCODE_LEFTBRACKET] ) lightList[selectLight].setAttenuationQuadratic(lightList[selectLight].getAttenuationQuadratic() + 0.001f);
	if ( keys[SDL_SCANCODE_RIGHTBRACKET] ) lightList[selectLight].setAttenuationQuadratic(lightList[selectLight].getAttenuationQuadratic() - 0.001f);

	
	// move particle cube
	if ( keys[SDL_SCANCODE_RIGHT] ) renderer.setParticleCubePosition(glm::vec3(renderer.getParticleCubePosition().x + 0.2, renderer.getParticleCubePosition().y, renderer.getParticleCubePosition().z));
	
	if ( keys[SDL_SCANCODE_UP] ) renderer.setParticleCubePosition(glm::vec3(renderer.getParticleCubePosition().x, renderer.getParticleCubePosition().y, renderer.getParticleCubePosition().z - 0.2));

	if ( keys[SDL_SCANCODE_LEFT] ) renderer.setParticleCubePosition(glm::vec3(renderer.getParticleCubePosition().x - 0.2, renderer.getParticleCubePosition().y, renderer.getParticleCubePosition().z));

	if ( keys[SDL_SCANCODE_DOWN] ) renderer.setParticleCubePosition(glm::vec3(renderer.getParticleCubePosition().x, renderer.getParticleCubePosition().y, renderer.getParticleCubePosition().z + 0.2));
	
	// move selected light
	if ( keys[SDL_SCANCODE_I] ) 
	{
		lightPosList[selectLight].z += -1.0f;
	}

	if ( keys[SDL_SCANCODE_K] )
	{
		lightPosList[selectLight].z += 1.0f;
	} 
	if ( keys[SDL_SCANCODE_J] )
	{
		 lightPosList[selectLight].x += -1.0f;
	}
	if ( keys[SDL_SCANCODE_L] )
	{
		lightPosList[selectLight].x += 1.0f;
	}

	if ( keys[SDL_SCANCODE_Y] )
	{
		lightPosList[selectLight].y += 1.0f;
	}
	
	if ( keys[SDL_SCANCODE_H] ) 
	{
		lightPosList[selectLight].y -= 1.0f;
	}

	// increase and decrease cone fall of for chosen light(spotlights only)
	if ( keys[SDL_SCANCODE_N])
	{
		lightList[selectLight].setConeFallOff(lightList[selectLight].getConeFallOff()-0.01);
	}
	if ( keys[SDL_SCANCODE_M]) 
	{
		lightList[selectLight].setConeFallOff(lightList[selectLight].getConeFallOff()+0.01);
	}

	if ( keys[SDL_SCANCODE_9])	
	{
		lightList[selectLight].turnOff();
	}

	if ( keys[SDL_SCANCODE_0])	
	{
		lightList[selectLight].turnOn();
	}

	if ( keys[SDL_SCANCODE_Z])
	{
		lightList[selectLight].setType(0);
	}
	if ( keys[SDL_SCANCODE_X])
	{
		lightList[selectLight].setType(1);
	}


	//fire particles
	if ( keys[SDL_SCANCODE_SPACE])
	{
		tempParticlePosition = std::rand() % 151;
		lightList[7].turnOn();
	}
	// update light with random particle
	lightPosList[7] = glm::vec4(renderer.getParticleLight()->getPositions(50).x,renderer.getParticleLight()->getPositions(50).y,renderer.getParticleLight()->getPositions(50).z,1.0);
	
	if(renderer.getParticleLight()->getParticleOn()==false) lightList[7].turnOff();

	return running;
}

void GameScreen::draw(SDL_Window * window) 
{
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, renderer.getFBO());
	glClear(GL_DEPTH_BUFFER_BIT);
	
	// Render small box 1
	renderer.renderDepth(glm::vec3(1.0f, 4.0f, -9.0f), glm::vec3(1.2f, 4.0f, 1.2f), lightPosList[0], lightConeDirection[0]);
	// Render small box 2
	renderer.renderDepth(glm::vec3(-5.0f, 1.0f, -4.0f), glm::vec3(3.0f, 1.2f, 1.2f), lightPosList[0], lightConeDirection[0]);
	// Render small box 3
	renderer.renderDepth(glm::vec3(-15.0f, 10.0f, -14.0f), glm::vec3(1.2f, 1.2f, 1.2f), lightPosList[0], lightConeDirection[0]);
	// Render small box 4
	renderer.renderDepth(glm::vec3(-20.0f, 1.0f, 0.0f), glm::vec3(1.2f, 1.2f, 1.2f), lightPosList[0], lightConeDirection[0]);
	// Render small box 5
	renderer.renderDepth(glm::vec3(-10.0f, 1.0f, -20.0f), glm::vec3(2.0f, 2.0f, 2.0f), lightPosList[0], lightConeDirection[0]);

	renderer.renderDepth(glm::vec3(-10.0f, -0.1f, -10.0f), glm::vec3(60.0f, 0.1f, 60.f), lightPosList[0], lightConeDirection[0]);
			
	//particle cube
	renderer.renderDepth(glm::vec3(renderer.getParticleCubePosition()), glm::vec3(1.0f,1.0f,1.0f), lightPosList[0], lightConeDirection[0]);

	glBindFramebuffer(GL_FRAMEBUFFER,0);
	// Render small box 1
	renderer.renderProp("crate", glm::vec3(1.0f, 4.0f, -9.0f), glm::vec3(1.2f, 4.0f, 1.2f),lightConeDirection, lightList, lightPosList);
	// Render small box 2
	renderer.renderProp("crate", glm::vec3(-5.0f, 1.0f, -4.0f), glm::vec3(3.0f, 1.2f, 1.2f), lightConeDirection, lightList, lightPosList);
	// Render small box 3
	renderer.renderProp("crate", glm::vec3(-15.0f, 10.0f, -14.0f), glm::vec3(1.2f, 1.2f, 1.2f), lightConeDirection, lightList, lightPosList);
	// Render small box 4
	renderer.renderProp("crate", glm::vec3(-20.0f, 1.0f, 0.0f), glm::vec3(1.2f, 1.2f, 1.2f), lightConeDirection, lightList, lightPosList);
	// Render small box 5
	renderer.renderProp("crate", glm::vec3(-10.0f, 1.0f, -20.0f), glm::vec3(2.0f, 2.0f, 2.0f), lightConeDirection, lightList, lightPosList);
		
	// Render particleCube
	renderer.renderProp("crate", glm::vec3(renderer.getParticleCubePosition()), glm::vec3(1.0f,1.0f,1.0f), lightConeDirection, lightList, lightPosList);

	// Render scene bottom
	renderer.renderProp("floor", glm::vec3(-10.0f, -0.1f, -10.0f), glm::vec3(60.0f, 0.1f, 60.0f), lightConeDirection, lightList, lightPosList);


	// Render list of lights
	renderer.renderLight(lightList, lightPosList);
	
	// Render a prop for each light
	for(int i = 0; i < NUMLIGHTS-1; i++)
	{
		renderer.renderLightProps(lightPosList[i]);
	}
	// render particles
	renderer.renderParticles("smoke", 10.0f,lightPosList[0]);

	// render HUD
	renderer.renderHUD();
	
	glDepthMask(GL_TRUE);

	SDL_GL_SwapWindow(window); // swap buffers
}